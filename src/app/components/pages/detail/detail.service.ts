
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailService {


  private API_SERVER =`https://akabab.github.io/superhero-api/api/id/`;
  constructor(private httpClient: HttpClient) { }
  
  public getHeroes(query:number):Observable<any>
  {
  return this.httpClient.get(`${this.API_SERVER}${query}.json`);
  }
}
