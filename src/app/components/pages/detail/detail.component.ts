import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Params } from '@angular/router';
import { take } from 'rxjs';
import { DetailService } from './detail.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  id!:number;
  heroe:any;
  constructor(public hero: DetailService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getHeroQuery();
  }
  private getHeroQuery():void
{
this.route.queryParams.subscribe(
  (params: Params)=>{
   this.id= params['q'];
  this.getHero();
})
}
private getHero():void{
  this.hero.getHeroes(this.id).subscribe
  (
    (data)=>{
      this.heroe=data; console.log(data)
    },(e)=> {console.error(e)}
    );
}

}
