import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { HeroesComponent } from './heroes/heroes.component';

const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch: 'full' },
  {path: 'home', component:HeroesComponent},
  {path:'details', component: DetailComponent}

  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
