import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class HeroesService {
   
    private API_SERVER = "https://akabab.github.io/superhero-api/api/all.json";
    constructor(private httpClient: HttpClient) { }
    
    public getHeroes():Observable<any>
    {
    return this.httpClient.get(this.API_SERVER);
    }
}

