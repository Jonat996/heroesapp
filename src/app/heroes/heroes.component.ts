import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeroesService } from './services/heroes.service';
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  title = 'project';
  heroe:any;
  p: number = 1;
  filterHeros="";
  constructor(public heroes:HeroesService, private router:Router){}
  ngOnInit() {
    this.heroes.getHeroes().subscribe
  (
    (data)=>{
      this.heroe=data; console.log(data)
    },(e)=> {console.error(e)}
    );
  }
  onSearch(value: number ):void{
    console.log("si")
    this.router.navigate(['/details'],{
      queryParams:{q:value}
    })
       
      console.log(value)
      
      }

}
