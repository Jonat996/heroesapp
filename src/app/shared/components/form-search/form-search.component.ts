import { Component, OnInit } from '@angular/core';
import { Params, Router } from '@angular/router';

@Component({
  selector: 'app-form-search',
  templateUrl: './form-search.component.html',
  styleUrls: ['./form-search.component.css']
})
export class FormSearchComponent implements OnInit {
name!:string;
filterHero="";
constructor( private router:Router) { }

  ngOnInit(): void {
  }
  onSearch(value: string ){
   if(value && value.length > 2){
this.router.navigate(['/details'],{
  queryParams:{q:value}
})
   }
  console.log(value)
    return value;
  }

}
