import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any):any {
    const resultHeros=[];
    for(const hero of value){
      if(hero.name.indexOf(arg) > -1){
        console.log('sip');
        resultHeros.push(hero);
      }
    }
    return resultHeros;
  }

}
